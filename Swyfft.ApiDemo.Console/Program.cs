﻿using System.Configuration;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Swyfft.ApiDemo.Console
{
    internal static class Program
    {
        private static readonly string _baseUrl = ConfigurationManager.AppSettings["SwyfftBaseUrl"];
        private static readonly string _apiKey = ConfigurationManager.AppSettings["SwyfftApiKey"];

        // ReSharper disable once UnusedParameter.Local
        static void Main(string[] args)
        {
            Task.Run(MainAsync).Wait();
        }

        static async Task MainAsync()
        {
            const string fullAddress = "20 Cedar Dr, Rochelle Park NJ 07662";
            const double livingSpace = 2500;

            var api = new SwyfftApiProxy(_baseUrl, _apiKey);
            var response = await api.GetQuote(fullAddress, livingSpace);

            System.Console.WriteLine(PrettifyJson(response));
        }

        private static string PrettifyJson(string json)
        {
            dynamic parsedJson = JsonConvert.DeserializeObject(json);
            return JsonConvert.SerializeObject(parsedJson, Formatting.Indented);
        }
    }
}
