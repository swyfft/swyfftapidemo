using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using RestSharp;

namespace Swyfft.ApiDemo.Console
{
    internal class SwyfftApiProxy
    {
        private readonly string _apiUrl;
        private readonly string _apiKey;

        public SwyfftApiProxy(string baseUrl, string apiKey)
        {
            _apiUrl = $"{baseUrl}/partnerapi/v1/";
            _apiKey = apiKey;
        }

        public async Task<string> GetQuote(string fullAddress, double? livingSpace)
        {
            const string resource = "quote";
            const Method method = Method.PUT;

            var data = new Dictionary<string, object>
            {
                ["fullAddress"] = fullAddress,
                ["livingSpace"] = livingSpace,
            };

            var response = await Execute(resource, method, data);
            return response.Content;
        }

        private async Task<IRestResponse> Execute(string resource, Method method, Dictionary<string, object> data)
        {
            var request = new RestRequest(resource, method)
                .AddHeader("X-SwyfftApiKey", _apiKey)
                .AddJsonBody(data);

            var client = new RestClient(_apiUrl);
            var response = await client.ExecuteTaskAsync(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new HttpException($"Failed to call '{_apiUrl}'.");
            }
            if (response.StatusCode == HttpStatusCode.Forbidden)
            {
                throw new HttpException($"Failed to call '{_apiUrl}'. Response: '{(int)response.StatusCode}. {response.StatusDescription}'.");
            }

            return response;
        }
    }
}
