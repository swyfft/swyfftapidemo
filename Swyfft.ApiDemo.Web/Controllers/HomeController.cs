﻿using System.Configuration;
using System.Web.Mvc;
using Swyfft.ApiDemo.Web.Models;

namespace Swyfft.ApiDemo.Web.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Message = "API User Interface";
            var model = new HomeModel
            {
                ApiKey = ConfigurationManager.AppSettings["SwyfftApiKey"],
                BaseUrl = ConfigurationManager.AppSettings["SwyfftBaseUrl"]
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult Widget()
        {
            ViewBag.Message = "API User Interface";
            var model = new HomeModel
            {
                ApiKey = ConfigurationManager.AppSettings["SwyfftApiKey"],
                BaseUrl = ConfigurationManager.AppSettings["SwyfftBaseUrl"]
            };
            return View(model);
        }

    }
}
