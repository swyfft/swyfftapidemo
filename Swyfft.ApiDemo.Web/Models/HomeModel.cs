﻿namespace Swyfft.ApiDemo.Web.Models
{
    public class HomeModel
    {
        public string ApiKey { get; set; }
        public string BaseUrl { get; set; }
    }
}